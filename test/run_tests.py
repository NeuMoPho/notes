#! /usr/bin/env python3
#                                                          07 FEB 2019
# 		Andrew J. Worth
# 		andy@neuromorphometrics.com
#
# 		Neuromorphometrics, Inc.
# 		22 Westminster Street
# 		Somerville, MA  02144-1630  USA
#
# 		http://neuromorphometrics.com
#
# *********************************************************************
#
#   (c) Copyright 2019 Neuromorphometrics      All rights reserved
#
# *********************************************************************

import os
import shutil

os.system("clear")

print("Testing updatePage.py and newpage.py")

# Copy the test input files into a clean directory
od = "origdir"
# {od} directory be like:
#
# {od}/index.html
# {od}/name with spaces.html
# {od}/dir with spaces/index.html
# {od}/dir with spaces/name with spaces.html
#
# Initialize inputs, first remove previous results
rd = "dir"
if os.access(rd, os.F_OK):
    os.system("rm -rf " + rd)
os.system("mkdir " + rd)
os.system(f"cp -r {od}/* {rd}")

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py with -k (keep original file):")
    os.system("../updatepage.py -k -v index.html")

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py -v (verbose):")
    os.system("../updatepage.py -v index.html")

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py (normal usage):")
    os.system("../updatepage.py index.html")

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py (spaces in file name):")
    os.system("../updatepage.py -v dir/name\ with\ spaces.html")

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py (spaces in directory name):")
    os.system("../updatepage.py -v dir/dir\ with\ spaces/index.html")

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py (spaces in directory and file name):")
    os.system("../updatepage.py -v " + "dir/dir\ with\ spaces/name\ with\ spaces.html")

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py (file:///Users/andy... browser path):")
    os.system(
        "../updatepage.py -v "
        + "file:///Users/andy/Development/src/NVMutilities/newpage/test/dir/index.html"
    )

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py (missing file):")
    os.system(
        "../updatepage.py -v "
        + "file:///Users/andy/Development/src/NVMutilities/newpage/test/dir/missing.html"
    )

if False:

    print("\n-------------------------------------------------------------")
    print("Testing newpage.py (file:///Users/andy... browser path):")
    os.system(
        "../newpage.py -v "
        + "-w file:///Users/andy/Development/src/NVMutilities/newpage/test/dir"
        + " fundir"
    )

if False:

    print("\n-------------------------------------------------------------")
    print("Testing newpage.py (don't overwrite existing):")
    os.system(
        "../newpage.py -v "
        + "-w file:///Users/andy/Development/src/NVMutilities/newpage/test dir"
    )

if False:

    print("\n-------------------------------------------------------------")
    print("Testing updatepage.py on new index with to do and time line links:")
    os.system(
        "../updatepage.py -v "
        + "file:///Users/andy/Development/src/NVMutilities/newpage/test/dir/index_with_newlinks/index.html"
    )

if False:

    print("\n-------------------------------------------------------------")
    print("Testing newpage.py (2 args now -w is old default):")
    os.system("../newpage.py -V " + "-w " + '"OOogy baloogy"')

if False:

    print("\n-------------------------------------------------------------")
    print(
        "Testing newpage.py (spaces become underscore, 2 args now -w is old default):"
    )
    os.system(
        "../newpage.py -V "
        + "file:///Users/andy/Development/src/NVMutilities/newpage/test/dir "
        + '"Kugga Bongo Bong a-haha"'
    )

if True:

    print("\n-------------------------------------------------------------")
    print(
        "Testing updatepage.py (add tidy):"
    )
    os.system("cp pages/Resumes_index.html Resumes_index.html")
    os.system("../updatepage.py -t Resumes_index.html")
    # clean up
    os.system("rm Resumes_index.html")
