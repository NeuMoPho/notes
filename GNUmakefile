######################################################################
#
#                           GNU Makefile
#
######################################################################
# 						           13 Sep 2001
# Neuromorphometrics
# 22 Westminster Street
# Somerville, MA 02144-1630
# Neuromorphometrics.com
#
# 24 November, 2008 - Adapting to run on a Macintosh
#*********************************************************************
#*								     *
#* (c) Copyright 2001-2008  Neuromorphometrics  All Rights Reserved  *
#*								     *
#*********************************************************************
# Usage: type "make" to make test versions of everything
#        type "make install" to put things into their permanent places
#
# You must have $TEMP_PATH before $F_BIN_PATH in your .bashrc $PATH
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#  How to customize this file to work for you:
#
#  Set the following variable to your user name:
#
MEMEME		= andy
#
# and modify HEADERS, LOC_HEADERS, LIB_SOURCES, LIBRARY, SOURCES,
# EXEC_SOURCES, SCRIPTS, INSTALLABLES, DOCS, EXTRA_INCLUDES,
# and EXTRA_LIBS below.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

# Installable Headers
# "make" (nothing)
# "make install" will instal these in $F_INC_PATH
#
HEADERS 	= 

# Local Headers
# "make" (nothing)
# "make install" will instal these in $F_INC_PATH
# Note: if you want a header to be installed, add it to HEADERS above
#
LOC_HEADERS 	= 




# Library Source Files
# "make" compiles them into $TEMP_PATH
# "make install" does nothing with them
#
LIB_SOURCES 	= 

# Library Name
# "make" compiles this into $TEMP_PATH
# "make install" copies it into $F_LIB_PATH
# Note: add library files that this project uses (but does not create)
# in EXTRA_LIBS below.
#
LIBRARY 	= 

# C Source Files (without "main()" routines)
# "make" compiles them into $TEMP_PATH
# "make install" does nothing with them.
#
SOURCES		=

# External-link C Source Files (without "main()" routines) that are 
# linked by programs that want to use the above libraries, but are not
# compiled with the code in this directory.  These files are an interface
# to the above libraries and can use global variables from the program
# that they link with and then pass these to the libraries.  "make"
# compiles them into $TEMP_PATH "make install" does nothing with them.
# These files must be linked with the external programs.
#
SRC_EXT		= 					

# Executable C Source Files (that DO have "main()" routines)
# "make" compiles them into $TEMP_PATH (using object code from $SOURCES
# and also the $LIBRARY above).
# Note: Add these to $INSTALLABLES below if they should be installed
# for everyone to use.
#
EXEC_SOURCES 	= 

# Shell Scripts
# "make" copies them into $TEMP_PATH and makes them executable.  Shell
# scripts are assumed to be in this directory named something.csh.  The
# executable versions do not have the suffix ".csh".
# Note: Add these (without the ".csh" suffix) to $INSTALLABLES below if
# they should be installed for everyone to use.
#
SCRIPTS		= 
SHSCRIPTS	= ejectall

# Python Scripts
# "make" copies them into $TEMP_PATH and makes them executable.  These
# scripts are assumed to be in this directory named something.py.  The
#
PY_SCRIPTS	= newpage.py updatepage.py openp.py gittyup.py colors.py

# Installable Executables
# "make" (nothing)
# "make install" copies them into $F_BIN_PATH and saves the original
# version (if any) in $F_BIN_PATH/old.
#
INSTALLABLES 	= 

# Installable Documentation
# "make" (nothing)
# "make install" copies them into $F_DOC_PATH
#
DOCS		= 

# Final Result Location
#
USROPT		= /Users/andy/Development

#THISOS 	= linux
THISOS 		= osx

# Paths
#
TEMP_PATH	= /$(USROPT)/bin/$(MEMEME)
F_INC_PATH	= /$(USROPT)/include
F_LIB_PATH	= /$(USROPT)/lib
F_BIN_PATH	= /$(USROPT)/bin
F_DOC_PATH	= /$(USROPT)/doc

# Extras
#
# Add things to the following three variables if they are needed to
# compile the code.  These will not be installed anywhere.
#
EXTRA_INCLUDES	= -I/$(USROPT)/include -I../NVMforCMA/gdf
EXTRA_LIBS 	= -lm 
#EXTRA_LIBS 	= -lm ../NVMforCMA/gdf/build/$(THISOS)/libNgdf.a
#EXTRA_LIBS 	= -lX11 -lgdf -lcma -lm

#========================================================================

# Source to object name translations
#
OBJECTS		= $(SOURCES:%.c=$(TEMP_PATH)/%.o)
OBJ_EXT		= $(SRC_EXT:%.c=$(TEMP_PATH)/%.o)
EXEC_OBJECTS	= $(EXEC_SOURCES:%.c=$(TEMP_PATH)/%)
LIB_OBJECTS 	= $(LIB_SOURCES:%.c=$(TEMP_PATH)/%.o)
TEST_LIB	= $(LIBRARY:%=$(TEMP_PATH)/%)
CSH_OBJECTS	= $(SCRIPTS:%.csh=$(TEMP_PATH)/%)
SH_OBJECTS	= $(SHSCRIPTS:%=$(TEMP_PATH)/%)
PY_OBJECTS	= $(PY_SCRIPTS:%.py=$(TEMP_PATH)/%.py)
EXECUTABLES 	= $(INSTALLABLES:%=$(F_BIN_PATH)/%)
FINAL_LIB 	= $(LIBRARY:%=$(F_LIB_PATH)/%)
FINAL_HDR 	= $(HEADERS:%=$(F_INC_PATH)/%)
FINAL_DOCS	= $(DOCS:%=$(F_DOC_PATH)/%)

# Compiler
#
CC		= gcc

# Flags
#
CFLAGS 		= -g $(EXTRA_INCLUDES)
CPPFLAGS	=
LDFLAGS		= -L/$(USROPT)/lib

#
# make library and standalone routines
#
all: $(LIB_OBJECTS) $(TEST_LIB) $(OBJ_EXT) $(OBJECTS) $(EXEC_OBJECTS) \
	$(CSH_OBJECTS) $(SH_OBJECTS) $(PY_OBJECTS)

$(LIB_OBJECTS): $(HEADERS) $(LOC_HEADERS)
$(LIB_OBJECTS): $(TEMP_PATH)/%.o : %.c
	@echo "Compiling library source code file:" $(@F:.o=.c)
	@$(CC) $(CFLAGS) $(CPPFLAGS) \
		-c $(@F:.o=.c) -o $@

$(TEST_LIB): $(HEADERS) $(LOC_HEADERS) $(LIB_OBJECTS) 
	@echo "Creating library:" $(@F)
	@ar rv $(TEST_LIB) $(LIB_OBJECTS) 
	@ranlib $(TEST_LIB) 

$(OBJECTS): $(HEADERS) $(LOC_HEADERS)
$(OBJECTS): $(TEMP_PATH)/%.o : ./%.c
	@echo "Compiling source code file:" $(@F:.o=.c)
	@$(CC) $(CFLAGS) $(CPPFLAGS) -c $(@F:.o=.c) -o $@

$(OBJ_EXT): $(HEADERS) $(LOC_HEADERS)
$(OBJ_EXT): $(TEMP_PATH)/%.o : %.c
	@echo "Compiling source code file for external use:" $(@F:.o=.c)
	@$(CC) $(CFLAGS) $(CPPFLAGS) -c $(@F:.o=.c) -o $@

$(EXEC_OBJECTS): $(HEADERS) $(LOC_HEADERS) $(OBJECTS) $(TEST_LIB)
$(EXEC_OBJECTS): $(TEMP_PATH)/%	: %.c
	@echo "Compiling:" $(@F)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(OBJECTS) \
		$(@F).c -o $@ $(TEST_LIB) $(EXTRA_LIBS)

$(CSH_OBJECTS): $(TEMP_PATH)/% : %.csh
	@echo "Making script executable:" $@
	@cp $(@F).csh $@
	@chmod 700 $@

$(SH_OBJECTS): $(TEMP_PATH)/% : %
	@echo "Making script executable:" $@
	@cp $(@F) $@
	@chmod 700 $@

$(PY_OBJECTS): $(TEMP_PATH)/%.py : %.py
	@echo "Making Python script executable:" $@
	@cp $(@F) $(@)
	@chmod 700 $(@)

#
# copy local (test) versions for everyone to use
#
install: $(FINAL_HDR) $(FINAL_LIB) $(EXECUTABLES)
	@echo " "
	@echo "Don't forget to add a short description of these commands"
	@echo "to the file: /usr/local/doc/commands."
	@echo " "

$(FINAL_HDR): $(HEADERS)
	@echo "Installing header:" $(@F)
	@cp $(@F) $@

$(FINAL_LIB): $(TEMP_PATH)/$$(@F)
	@echo "Installing library:" $(@F)
	@cp $(TEMP_PATH)/$(@F) $@

$(EXECUTABLES): $(TEMP_PATH)/% : $(TEMP_PATH)/%
	@echo "Installing:" $(@F)
	@touch $@
	@mv $@ $(TEMP_PATH)/old/$(@F)
	@cp $(TEMP_PATH)/$(@F) $@
	@chmod 755 $@

echo:
	@echo "echoing variables:"
	@echo MEMEME $(MEMEME)
	@echo USROPT $(USROPT)
	@echo CC $(CC)
	@echo CFLAGS $(CFLAGS)
	@echo CPPFLAGS $(CPPFLAGS)
	@echo LDFLAGS $(LDFLAGS)
	@echo " "
	@echo HEADERS $(HEADERS)
	@echo LOC_HEADERS $(LOC_HEADERS)
	@echo LIB_SOURCES $(LIB_SOURCES)
	@echo LIBRARY $(LIBRARY)
	@echo SOURCES $(SOURCES)
	@echo EXEC_SOURCES $(EXEC_SOURCES)
	@echo SCRIPTS $(SCRIPTS)
	@echo INSTALLABLES $(INSTALLABLES)
	@echo DOCS $(DOCS)
	@echo " "
	@echo EXTRA_INCLUDES $(EXTRA_INCLUDES)
	@echo EXTRA_LIBS $(EXTRA_LIBS)
	@echo " "
	@echo TEMP_PATH $(TEMP_PATH)
	@echo F_INC_PATH $(F_INC_PATH)
	@echo F_LIB_PATH $(F_LIB_PATH)
	@echo F_BIN_PATH $(F_BIN_PATH)
	@echo F_DOC_PATH $(F_DOC_PATH)
	@echo OBJECTS $(OBJECTS)
	@echo OBJ_EXT $(OBJ_EXT)
	@echo EXEC_OBJECTS $(EXEC_OBJECTS)
	@echo LIB_OBJECTS $(LIB_OBJECTS)
	@echo TEST_LIB $(TEST_LIB)
	@echo CSH_OBJECTS $(CSH_OBJECTS)
	@echo SH_OBJECTS $(CSH_OBJECTS)
	@echo PY_OBJECTS $(PY_OBJECTS)
	@echo EXECUTABLES $(EXECUTABLES)
	@echo FINAL_LIB $(FINAL_LIB)
	@echo FINAL_HDR $(FINAL_HDR)
	@echo FINAL_DOCS $(FINAL_DOCS)
