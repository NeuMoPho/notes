#!/usr/bin/env python3
"""
SYNOPSIS

    newpage.py [options] WHERE NAME
    
    Where
       WHERE - path to where new page should be created
       NAME  - title of new page to create

    Default location of new page: NMM/Notes/Customers (see -w option)

DESCRIPTION

    Creates a new html page with the given NAME and opens it in
    SeaMonkey and a Finder window.  The name of the html file will
    be created from only the alphanumeric characters in the given
    NAME.  In an existing html file, adding a link to the newly
    created html file can easily be done by command-k and "choose
    file", then drag the html file from the Finder window.

    Edit at Development/src/NVMutilities/newpage

EXAMPLES

    (see tests)

AUTHOR

    Andrew J. Worth 
    andy@neuromorphometrics.com
         
    Neuromorphometrics, Inc.
    22 Westminster Street
    Somerville, MA  02144-1630  USA
     
    http://Neuromorphometrics.com
    
LICENSE
    (c) Copyright 2018-2020 Neuromorphometrics, Inc.   All rights reserved.

VERSION
"""

import sys
import os
import traceback
import optparse
import time
from datetime import timedelta, datetime
import xml.etree.ElementTree as ET
import glob


GVersion = "$Id: newpage.py,v 0.0 2013/04/11 04:19:15 andy Exp $"
GStartTime = time.time()
GVerboseLevel = 0

WHERE = "/Users/andy/NMM/Notes/Customers/"


# -----------------------------------------------------------------------------
def PrintInColor(f, b, t, e):
    if f >= 0:
        print("\x1b[38;5;" + str(f) + "m", end="")  # set Foreground
    if b >= 0:
        print("\x1b[48;5;" + str(b) + "m", end="")  # set Background
    print(t + "\u001b[0m", end=e)  # Reset
    # print('\x1b[38;5;0m', end='')   # Black Foreground
    # print('\x1b[48;5;231m', end='') # White Background


# -----------------------------------------------------------------------------
def DoIt(cmd):
    if GVerboseLevel >= 0:
        PrintInColor(129, -1, cmd, "\n")  # purple
        print()
    if options.dryrun:
        print("NOT REALLY RUNNING " + cmd)
    else:
        os.system(cmd)


# ===============================================================================
def main():

    global options, args, GVersion, GStartTime, GVerboseLevel

    # I'm just wondering who I really am
    FullPathToMe = os.path.realpath(__file__)
    # print("FullPathToMe ", FullPathToMe)
    SplitFullPathToMe = os.path.split(FullPathToMe)
    PathToMe = SplitFullPathToMe[0]
    ME = SplitFullPathToMe[1]
    sPathToMe = PathToMe.split("/")
    ResPath = "/".join(sPathToMe[:-1]) + "/../"
    CurWurkDur = os.getcwd()  # Where I'm being run from

    if GVerboseLevel >= 2:
        print('  I am "%s"' % (ME))
        print('  Some call me "%s"' % (__name__))
        print('  I am at "%s"' % (PathToMe))
        print('  I was run from "%s"' % (CurWurkDur))
        print("  args=" + repr(args))

    nargs = len(args)
    if nargs == 1:
        if options.where:
            name = args[0]
            where = WHERE
            print('Setting where to : "' + where + '"')
    elif nargs == 2:
        if options.where:
            print("Ignoring -w since you gave two arguments, WTF?")
        where = args[0]
        name = args[1]
    else:
        parser.print_help()
        s = "Expected 2 arguments, found " + str(nargs)
        PrintInColor(9, -1, s, "\n")  # red
        print("Exiting Early.")
        sys.exit(1)

    if where[-1:] != "/":
        where += "/"
        # print('"'+where+'"')

    if where[:7] == "file://":
        where = where[7:]
        print(f'Removed "file://" from path. New path:\n {where}')

    nameNoSpaces = name.replace(" ", "_")
    nameNoJunk = "".join(x for x in nameNoSpaces if (x.isalnum() or x in "-_"))
    fullDir = where + nameNoJunk

    # -------------------------------------------------------------------------
    if GVerboseLevel >= 1:
        print(ME + " starting...")
        print(' name      ="' + name + '"')
        print(' nameNoJunk="' + nameNoJunk + '"')
        print(' where     ="' + where + '"')

    if os.access(fullDir, os.F_OK) == False:
        print("Making directory " + fullDir)
        os.makedirs(fullDir)
    else:
        print("Using existing directory " + fullDir)

    picsDir = fullDir + "/pics"
    if os.access(picsDir, os.F_OK) == False:
        print("Making directory " + picsDir)
        os.makedirs(picsDir)
    else:
        print("Using existing directory " + picsDir)

    fn = fullDir + "/index.html"
    if os.path.isfile(fn):
        print("File already exists: " + fn)
    else:
        f = open(fn, "w")
        P1 = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
"""
        f.write(P1)
        f.write("    <title>" + name + "</title>\n")
        P2 = """  </head>
  <body>
"""
        f.write(P2)
        f.write("    <h1>" + name + "</h1>\n")
        P3 = """    <p><a href="#To_Do">To Do</a>&nbsp;&nbsp;&nbsp;&nbsp; <a
        href="#Time_Line">Time Line</a>.<br>
    </p>
    <p><br>
    </p>
        <h2><a name="To_Do"></a>To Do</h2>
    <ul>
      <li><br>
      </li>
    </ul>
    <h2><a name="Time_Line"></a>Time Line</h2>
"""
        f.write(P3)
        aFile = os.popen("date")
        date = aFile.read()[:-1]
        aFile.close()
        print(date)
        namedAnchor = date.replace(" ", "_")
        print(namedAnchor)
        YYYYMMDD = datetime.now().strftime("%Y-%m-%d")
        f.write(
            '    <ul>\n      <li><a href="#'
            + namedAnchor
            + '">'
            + YYYYMMDD
            + "</a> Started this file<br>\n"
        )
        P5 = """      </li>
    </ul>
    <hr width="100%" size="2">
"""
        f.write(P5)

        f.write('    <b><a name="' + namedAnchor + '"></a>' + date + "</b>\n")
        P4 = """    <br>
    <br>
    <br>
    <br>
    -- EOF --<br>
    <p><br>
    </p>
  </body>
</html>
"""
        f.write(P4)
        f.close()

    cmd = 'open "' + fullDir + '"'
    DoIt(cmd)
    cmd = 'open -a SeaMonkey "' + fn + '"'
    DoIt(cmd)

    # PrintInColor(231,196,' Exiting Early ','') ; print('') # white on red
    # sys.exit(0)

    if GVerboseLevel >= 1:
        print(ME + " End of main()")


# ---------------------------------------------------------------------------
# Call Main Program (above) with exceptions, etc.
# ---------------------------------------------------------------------------
# Uncomment the following section if you want readline history support.
# import readline, atexit
# histfile = os.path.join(os.environ['HOME'], '.RunAll_history')
# try:
#    readline.read_history_file(histfile)
# except IOError:
#    pass
# atexit.register(readline.write_history_file, histfile)
if __name__ == "__main__":

    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()["__doc__"],
            version=GVersion,
        )

        # version='$Id: RunAll.py,v 1.4 2013/04/11 04:19:15 andy Exp $')
        parser.add_option(
            "-d",
            "--dryrun",
            action="store_true",
            default=False,
            help="don't execute, just show what doing",
        )
        parser.add_option(
            "-q", "--quiet", action="store_true", default=False, help="suppress output"
        )
        parser.add_option(
            "-v", "--verbose", action="store_true", default=False, help="verbose output"
        )
        parser.add_option(
            "-V",
            "--Verbose",
            action="store_true",
            default=False,
            help="more verbose output",
        )
        parser.add_option(
            "-l",
            "--verboseLevel",
            action="store",
            type="int",
            default=0,
            help="level of verbose output",
        )
        parser.add_option(
            "-w",
            "--where",
            action="store_true",
            default=False,
            help=f'put new page at "{WHERE}"',
        )
        (options, args) = parser.parse_args()

        GVerboseLevel = options.verboseLevel
        if options.quiet:
            GVerboseLevel = -1
        if options.verbose:
            if GVerboseLevel < 1:
                GVerboseLevel = 1
        if options.Verbose:
            if GVerboseLevel < 2:
                GVerboseLevel = 2
        if GVerboseLevel > 0:
            print("Running: " + __file__)
            print(time.ctime(GStartTime))
            print("VerboseLevel is " + str(GVerboseLevel))

        exit_code = main()  # DoItToIt!

        if exit_code is None:
            exit_code = 0
            tt = "Total time: %s" % timedelta(seconds=(time.time() - GStartTime))
            if GVerboseLevel >= 1:
                print(time.asctime())
                print(tt)

        sys.exit(exit_code)

    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print("ERROR, UNEXPECTED EXCEPTION")
        print(str(e))
        traceback.print_exc()
        os._exit(1)
