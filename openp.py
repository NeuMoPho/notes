#!/usr/bin/env python3
"""Given the full path to a file, open the path and ignore the file."""

import sys
import os


print(sys.argv)
path = os.path.split(sys.argv[1])[0]
print(path)
cmd = f'open "{path}"'
print(cmd)
os.system(cmd)
