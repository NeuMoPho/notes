#!/usr/bin/env python3
"""
SYNOPSIS

    updatepage.py [options] path/to/page.html
    
    Where
       path/to/page.html - full path to the page to update

DESCRIPTION

    Creates a new html page with the given NAME and opens it in
    SeaMonkey and a Finder window.  

    Edit at Development/src/NVMutilities/updatepage

EXAMPLES

    TODO

AUTHOR

    Andrew J. Worth 
    andy@neuromorphometrics.com
         
    Neuromorphometrics, Inc.
    22 Westminster Street
    Somerville, MA  02144-1630  USA
     
    http://Neuromorphometrics.com
    
LICENSE
    (c) Copyright 2019 Neuromorphometrics, Inc.   All rights reserved.

VERSION
"""

import sys
import os
import traceback
import optparse
import time
from datetime import timedelta, datetime
import xml.etree.ElementTree as ET
import glob

GVersion = "$Id: updatepage.py,v 0.0 2013/04/11 04:19:15 andy Exp $"
GStartTime = time.time()
GVerboseLevel = 0

# -----------------------------------------------------------------------------
def PrintInColor(f, b, t, e):
    if f > -1:
        print("\x1b[38;5;" + str(f) + "m", end="")  # set Foreground
    if b > -1:
        print("\x1b[48;5;" + str(b) + "m", end="")  # set Background
    print(t + "\u001b[0m", end=e)  # Reset
    # PrintInColor(0,202,'\x1b[5m'+msg,'') ; # blinking black on orange


# -----------------------------------------------------------------------------
def DoIt(cmd):
    if GVerboseLevel >= 0:
        PrintInColor(129, -1, cmd, "\n")  # purple
    if options.dryrun:
        print("NOT REALLY RUNNING " + cmd)
    else:
        os.system(cmd)


# -----------------------------------------------------------------------------
def ComplainAndDie(msg):
    PrintInColor(9, -1, msg, "\n")  # red
    print("Exiting Early.")
    sys.exit(0)


# ===============================================================================
def main():

    global options, args, GVersion, GStartTime, GVerboseLevel

    # Does anybody really know what timne it is?
    YYYYMMDD_HHMMSS = datetime.now().strftime("%Y-%m-%d_%H%M%S")

    # I'm just wondering who I really am
    FullPathToMe = os.path.realpath(__file__)
    # print("FullPathToMe ", FullPathToMe)
    SplitFullPathToMe = os.path.split(FullPathToMe)
    PathToMe = SplitFullPathToMe[0]
    ME = SplitFullPathToMe[1]
    sPathToMe = PathToMe.split("/")
    ResPath = "/".join(sPathToMe[:-1]) + "/../"
    CurWurkDur = os.getcwd()  # Where I'm being run from

    if GVerboseLevel >= 2:
        print('  I am "%s"' % (ME))
        print('  Some call me "%s"' % (__name__))
        print('  I am at "%s"' % (PathToMe))
        print('  I was run from "%s"' % (CurWurkDur))
        print("  args=" + repr(args))

    # -------------------------------------------------------------------------

    nargs = len(args)
    if nargs != 1:
        parser.print_help()
        ComplainAndDie("Expected 1 argument, found " + str(nargs))

    thePage = args[0].replace("%20", " ")
    (base, ext) = os.path.splitext(thePage)
    path, fname = os.path.split(thePage)

    if path == "":
        path = "./"
    elif path[:7] == "file://":
        path = path[7:]
        base = base[7:]
        thePage = thePage[7:]

    if os.access(path, os.F_OK) == False:
        ComplainAndDie(f"ERROR, nonexistent: {path}")
    else:
        if GVerboseLevel >= 1:
            print(f'Yay, path exists: "{path}"')

    if os.access(thePage, os.F_OK) == False:
        ComplainAndDie(f"ERROR, nonexistent: {thePage}")
    else:
        if GVerboseLevel >= 1:
            print(f'Yay, thePage exists: "{thePage}"')

    aPipe = os.popen("date")
    date = aPipe.read().rstrip()
    aPipe.close()
    tag = date.replace(" ", "_")
    YYYYMMDD = datetime.now().strftime("%Y-%m-%d")

    if GVerboseLevel >= 1:
        print(ME + " starting...")
        print(f' thePage  = "{thePage}"')
        print(f' base     = "{base}"')
        print(f' ext      = "{ext}"')
        print(f' path     = "{path}"')
        print(f' fname    = "{fname}"')
        print(f' date     = "{date}"')
        print(f' tag      = "{tag}"')
        print(f' YYYYMMDD = "{YYYYMMDD}"')

    if options.tidy:
        orig_file = base + "_" + YYYYMMDD_HHMMSS + "_orig" + ext
        cmd = f"mv {thePage} {orig_file}"
        DoIt(cmd)
        cmd = f"tidy {orig_file} > {thePage}"
        DoIt(cmd)

    keepFile = base + "_" + YYYYMMDD_HHMMSS + ext
    outF = open(keepFile, "w")

    lookingForTL = True
    lookingForUL = False
    lookingForEOF = False
    foundTL = -1
    foundUL = -1
    foundEOF = -1

    with open(thePage, "r") as f:
        for ll, line in enumerate(f):

            # first look for Time Line line
            if lookingForTL:
                outF.write(
                    line
                )  # write every line until then, including Time Line line
                if "Time Line</h2>" in line:
                    print(f'found "Time Line</h2>" in line {ll}')
                    lookingForTL = False  # found it
                    foundTL = ll
                    lookingForUL = True
            elif lookingForUL:
                outF.write(line)  # write every line until then, including UL line
                if "<ul>" in line:
                    print(f"found <ul> in line {ll}")
                    # Write new line with link to end
                    outF.write(
                        '<li><a href="#'
                        + tag
                        + '">'
                        + YYYYMMDD
                        + "</a> updating</li>\n"
                    )
                    lookingForUL = False  # found it
                    foundUL = ll
                    lookingForEOF = True
            elif lookingForEOF:
                if "-- EOF --" in line:
                    print(f"found -- EOF -- in line {ll}")
                    s = '    <hr width="100%" size="2">\n'
                    s += '<b><a name="' + tag + '">\n'
                    s += "</a>" + date + "</b><br>\n"
                    s += "<br>\n"
                    s += "<br>\n"
                    outF.write(s)
                    outF.write(line)  # write EOF line
                    lookingForEOF = False  # Found it
                    foundEOF = ll
                else:
                    outF.write(line)  # write every line until EOF found
            else:
                outF.write(line)  # write rest of file and quite
    outF.close()

    allGood = True
    if foundTL < 0:
        msg = 'Could not find "Time Line"'
        PrintInColor(202, -1, msg, "\n")  # orange
        allGood = False
    if foundUL < 0:
        msg = 'Could not find "<ul>"'
        PrintInColor(202, -1, msg, "\n")  # orange
        allGood = False
    if foundEOF < 0:
        msg = 'Could not find "EOF"'
        PrintInColor(202, -1, msg, "\n")  # orange
        allGood = False

    # Save the original with a time+date stamp name
    # and replace the original with the updated file
    cmd = f'mv "{keepFile}" "{keepFile}.tmp" ; ' + \
          f'mv "{thePage}" "{keepFile}" ; ' +  \
          f'mv "{keepFile}.tmp" "{thePage}"'
    DoIt(cmd)

    # Show what changed
    cmd = 'diff -c "' + keepFile + '" "' + thePage + '"'
    DoIt(cmd)

    # Open a finder window that holds the page
    cmd = 'open "' + path + '"'
    DoIt(cmd)
    # and finally, open the page in Seamonkey so it can be edited
    cmd = 'open -a SeaMonkey "' + thePage + '"'
    DoIt(cmd)

    # Delete temporary file unless --keep or an error occurred
    if options.keep:
        msg = f"Keeping original file as {keepFile}"
        PrintInColor(2, -1, msg, "\n")  # green
    else:
        # ask before deleting
        msg = 'Press "y" to delete original file'
        PrintInColor(0, 202, "\x1b[5m" + msg, "")
        # blinking black on orange
        ans = input(" delete? [y/N]")
        print("\x1b[2F")  # move up to blinking line
        print("\x1b[0K")  # Clear to end of line to stop that damn flashing
        if ans == "y":
            cmd = 'rm "' + keepFile + '"'
            if allGood:
                print(f"OK, deleting {keepFile}")
                DoIt(cmd)
            if options.tidy:
                cmd = f"rm {orig_file}"
                DoIt(cmd)
        else:
            print(f"OK, I did NOT delete {keepFile}\nIn {path}")

    # PrintInColor(231,196,' Exiting Early ','') ; print('') # white on red
    # sys.exit(0)

    if GVerboseLevel >= 1:
        print(ME + " End of main()")


# ---------------------------------------------------------------------------
# Call Main Program (above) with exceptions, etc.
# ---------------------------------------------------------------------------
# Uncomment the following section if you want readline history support.
# import readline, atexit
# histfile = os.path.join(os.environ['HOME'], '.RunAll_history')
# try:
#    readline.read_history_file(histfile)
# except IOError:
#    pass
# atexit.register(readline.write_history_file, histfile)
if __name__ == "__main__":
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()["__doc__"],
            version=GVersion,
        )
        # version='$Id: RunAll.py,v 1.4 2013/04/11 04:19:15 andy Exp $')
        parser.add_option(
            "-k",
            "--keep",
            action="store_true",
            default=False,
            help="Keep re-named original file",
        )
        parser.add_option(
            "-d",
            "--dryrun",
            action="store_true",
            default=False,
            help="don't execute, just show what doing",
        )
        parser.add_option(
            "-t",
            "--tidy",
            action="store_true",
            default=False,
            help="pipe through tidy before modifying",
        )
        parser.add_option(
            "-q", "--quiet", action="store_true", default=False, help="suppress output"
        )
        parser.add_option(
            "-v", "--verbose", action="store_true", default=False, help="verbose output"
        )
        parser.add_option(
            "-V",
            "--Verbose",
            action="store_true",
            default=False,
            help="more verbose output",
        )
        parser.add_option(
            "-l",
            "--verboseLevel",
            action="store",
            type="int",
            default=0,
            help="level of verbose output",
        )
        (options, args) = parser.parse_args()
        # if len(args) < 1:
        #    parser.error ('missing argument')
        GVerboseLevel = options.verboseLevel
        if options.quiet:
            GVerboseLevel = -1
        if options.verbose:
            if GVerboseLevel < 1:
                GVerboseLevel = 1
        if options.Verbose:
            if GVerboseLevel < 2:
                GVerboseLevel = 2
        if GVerboseLevel > 0:
            print("Running: " + __file__)
            print(time.ctime(GStartTime))
            print("VerboseLevel is " + str(GVerboseLevel))
        exit_code = main()  # DoItToIt!
        if exit_code is None:
            exit_code = 0
            tt = "Total time: %s" % timedelta(seconds=(time.time() - GStartTime))
            if GVerboseLevel >= 1:
                print(time.asctime())
                print(tt)
        sys.exit(exit_code)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print("ERROR, UNEXPECTED EXCEPTION")
        print(str(e))
        traceback.print_exc()
        os._exit(1)
