#!/usr/bin/env python3
"""Git status or update for all of my git repos in a folder."""

from pathlib import Path
import argparse
import os


def main():

    exit_code = 0

    src = 'TEST/tests/'/Path(args.src)
    dst = 'TEST/tests/'/Path(args.dst)

    if args.verbose > 2:
      print(f'args.verbose = {args.verbose}')
      print('Three, being the third number')
    elif args.verbose > 1:
      print(f'args.verbose = {args.verbose}')
    else:
      print(f"no telling what verbose is.  Okay it's {args.verbose}")

    if args.verbose > 0:
        print(f'src {src}')
        print(f'dst {dst}')

    newfile, oldfile = args.compare

    print('print(args)')
    print(args)

    return exit_code

 
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)
    #parser.add_argument("src",help="(old) source test direcory name (no path)")
    #parser.add_argument("dst",help="(new) destination directory name")
    #parser.add_argument('--session-id', action='store', nargs='*', type=str,
    #                    help='filter input dataset by session id')
    #parser.add_argument('-c', '--compare', nargs=2, metavar=('new', 'old'), 
    #                    help='Compares previous run results in oldfile with ' +\
    #                    'actual run results in newfile.')
    parser.add_argument("-c", "--colors", action="store_true",
                        help="show colors")
    parser.add_argument('-f','--foo', type=int, default='3',
                        help='foo are you, foo foo, foo foo')
    parser.add_argument('-l', '--float', type=float, help='float on, floater!',
                        default='3.1415926')
    parser.add_argument('-v', '--verbose', action='count', default=0)

    args = parser.parse_args()

    if args.colors:
        test_colors()
        exit_code = 0

    else:
        exit_code = main()

    os.sys.exit(exit_code)
