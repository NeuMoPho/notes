#!/usr/bin/env python3

import os
import argparse
import json
import random

# See https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
FOR = "\x1b[38;5;"
BAC = "\x1b[48;5;"
RGBFOR = "\x1b[38;2;" # doesn't work
RGBBAC = "\x1b[48;2;" # doesn't work

C0 = "\x1b[0m"  # Reset
CRD = "\x1b[38;5;1m"  # Red dim
CR = "\x1b[38;5;9m"  # Red bold
CGD = "\x1b[38;5;2m"  # Green
CG = "\x1b[38;5;10m"  # Green
CYD = "\x1b[38;5;3m"  # Yellow
CY = "\x1b[38;5;11m"  # Yellow
CPD = "\x1b[38;5;4m"  # purple
CP = "\x1b[38;5;12m"  # purple
CPL = "\x1b[94m"  # purple light
CB = "\x1b[38;5;27m"  # Blue
CMD = "\x1b[38;5;5m"  # Magenta 5
CM = "\x1b[38;5;13m"  # Magenta 13
CCD = "\x1b[38;5;6m"  # Cyan
CC = "\x1b[38;5;14m"  # Cyan
CO = "\x1b[38;5;202m"  # Orange
CWR = "\x1b[38;5;231m\x1b[48;5;196m"  # White on Red

BOLD = "\x1b[1m"
DIM = "\x1b[2m"
ITALIC = "\x1b[3m"
UNDERLINED = "\x1b[4m"
BLINK = "\x1b[5m"
FASTBLINK = "\x1b[6m" # does not work
REVERSE = "\x1b[7m"
HIDDEN = "\x1b[8m" # doesn't print anything

UP = "\x1b[2F"
CLEAR = "\x1b[0K"

# use colors like:
# print(f"{CY}Yellow Text{C0}")
# msg = f"PaleVioletRed1 211 on {UNDERLINED}SkyBlue2 111"
# print(f"{FOR}211m{BAC}111m{msg}{C0}")


def colors():

    print(CRD + " Red 1 " + C0 + " normal")
    print(CR + " Red 9 " + C0 + " normal")
    print(CGD + " green 2 " + C0 + " normal")
    print(CG + " green 10 " + C0 + " normal")
    print(CYD + " yellow 3 " + C0 + " normal")
    print(CY + " yellow 11 " + C0 + " normal")
    print(CPD + " purple 4 " + C0 + " normal")
    print(CP + " purple 12 " + C0 + " normal")
    print(CB + " blue 27 " + C0 + " normal")
    print(CMD + " magenta 5 " + C0 + " normal")
    print(CM + " magenta 13 " + C0 + " normal")
    print(CCD + " Cyan 6 " + C0 + " normal")
    print(CC + " Cyan 14 " + C0 + " normal")

    print()
    print(CO + "orange 202" + C0 + " oink")
    print(CWR + " white on red " + C0 + " oink")

    print()
    print(CRD + BOLD + " Red 1 bold " + C0 + " -- ", end="")
    print(CGD + BOLD + " Green 2 bold " + C0)
    print(CR + BOLD + " Red 9 bold " + C0 + " -- ", end="")
    print(CG + BOLD + " Green 10 bold " + C0)
    print(CRD + DIM + " Red 1 dim " + C0 + " -- ", end="")
    print(CGD + DIM + " Green 2 dim " + C0)
    print(CR + DIM + " Red 9 dim " + C0 + " -- ", end="")
    print(CG + DIM + " Green 10 dim " + C0)
    print(CRD + UNDERLINED + " Red 1 underlined " + C0 + " -- ", end="")
    print(CGD + UNDERLINED + " Green 2 underlined " + C0)
    print(CR + UNDERLINED + " Red 9 underlined " + C0 + " -- ", end="")
    print(CG + UNDERLINED + " Green 10 underlined " + C0)
    print(CRD + BLINK + " Red 1 blink " + C0 + " -- ", end="")
    print(CGD + BLINK + " Green 2 blink " + C0)
    print(CR + BLINK + " Red 9 blink " + C0 + " -- ", end="")
    print(CG + BLINK + " Green 10 blink " + C0)
    print(CRD + REVERSE + " Red 1 reverse " + C0 + " -- ", end="")
    print(CGD + REVERSE + " Green 2 reverse " + C0)
    print(CR + REVERSE + " Red 9 reverse " + C0 + " -- ", end="")
    print(CG + REVERSE + " Green 10 reverse " + C0)
    print(CRD + HIDDEN + " Red 1 hidden " + C0 + " -- ", end="")
    print(CGD + HIDDEN + " Green 2 hidden " + C0)
    print(CR + HIDDEN + " Red 9 hidden " + C0 + " -- ", end="")
    print(CG + HIDDEN + " Green 10 hidden " + C0)
    print(CRD + BLINK + REVERSE + " Red 1 blink reverse " + C0 + " -- ", end="")
    print(CGD + BLINK + REVERSE + " Green 2 blink reverse " + C0)
    print(CR + BLINK + REVERSE + " Red 9 blink reverse " + C0 + " -- ", end="")
    print(CG + BLINK + REVERSE + " Green 10 blink reverse " + C0)
    print(CO + ITALIC + REVERSE + " Orange 202 italic reverse " + C0)
    msg = f"PaleVioletRed1 211 on {UNDERLINED}SkyBlue2 111"
    print(f"{FOR}211m{BAC}111m{msg}{C0}")


def all_colors():

    print()
    FullPathToMe = os.path.split(os.path.realpath(__file__))[0]
    print("FullPathToMe ", FullPathToMe)
    with open(FullPathToMe + "/colors.json") as jfp:
        colors = json.load(jfp)
    # print(json.dumps(colors, indent=4))

    C = "\x1b[38;5;"
    for col in colors:
        print(f"{C}{col['colorId']}m {col['name']} {col['colorId']} {C0}", end="")
        print(
            f"{REVERSE}{C}{col['colorId']}m {col['name']} {col['colorId']} {C0}", end=""
        )
        print()


def blink():

    print()
    print(f"{FOR}170m{BLINK} Orchid 170{C0}")
    ans = input("Stop the blinking? (y/n) ")
    if ans in ["y", "Y", "yes"]:
        print(UP + CLEAR)
        print(f"There you go.{CLEAR}")
    else:
        print("Okay boomer.")


def rgb(fr, fg, fb, br, bg, bb, txt):
    """This works for an xterm on a Mac but gets closest color in Terminal."""
    print(f"{RGBFOR}{fr};{fg};{fb}m{RGBBAC}{br};{bg};{bb}m{txt}{C0} set both")
    print(f"{RGBFOR}{fr};{fg};{fb}m{txt}{C0} set for only")
    print(f"{RGBBAC}{br};{bg};{bb}m{txt}{C0} set bac only")


def call_rgb():
    rgb(64,195,208, 95,0,175, "•••••••••hi ho")
    rgb(0,0,0, 255,255,255, "••••••••• black on white")
    rgb(255,0,0, 0,0,255, "•••••••••hi ho")
    # https://en.wikipedia.org/wiki/Cosmic_latte
    rgb(255,248,231, 0,0,0, "••••••••• Cosmic latte on black")
    rgb(255,248,231, 255,255,255, "••••••••• Cosmic latte on white")
    rgb(0,0,0, 255,248,231, "••••••••• black on Cosmic latte")
    rgb(255,255,255, 255,248,231, "••••••••• white on Cosmic latte")

    print("Set forground and background, print bullet")
    for ii in range(1000):
        fr = random.randint(0,255)
        fg = random.randint(0,255)
        fb = random.randint(0,255)
        br = random.randint(0,255)
        bg = random.randint(0,255)
        bb = random.randint(0,255)
        print(f"{RGBFOR}{fr};{fg};{fb}m{RGBBAC}{br};{bg};{bb}m•{C0}",end="")
    print("\nSet forground only, print bullet")
    for ii in range(1000):
        fr = random.randint(0,255)
        fg = random.randint(0,255)
        fb = random.randint(0,255)
        print(f"{RGBFOR}{fr};{fg};{fb}m•{C0}",end="")
    print("\nSet forground and background, print space")
    for ii in range(1000):
        fr = random.randint(0,255)
        fg = random.randint(0,255)
        fb = random.randint(0,255)
        br = random.randint(0,255)
        bg = random.randint(0,255)
        bb = random.randint(0,255)
        print(f"{RGBFOR}{fr};{fg};{fb}m{RGBBAC}{br};{bg};{bb}m {C0}",end="")
    print("\nSet forground only, print space")
    for ii in range(1000):
        fr = random.randint(0,255)
        fg = random.randint(0,255)
        fb = random.randint(0,255)
        print(f"{RGBFOR}{fr};{fg};{fb}m {C0}",end="")
    print("\nSet background only, print space")
    for ii in range(1000):
        br = random.randint(0,255)
        bg = random.randint(0,255)
        bb = random.randint(0,255)
        print(f"{RGBBAC}{br};{bg};{bb}m {C0}",end="")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-r", "--rgb", action="store_true", help="show rgb color"
    )
    parser.add_argument(
        "-a", "--all", action="store_true", help="show all colors and names"
    )
    parser.add_argument(
        "-b",
        "--blink",
        action="store_true",
        help="show blinking line and then clear it",
    )

    args = parser.parse_args()

    colors()

    if args.rgb:
        call_rgb()

    if args.all:
        all_colors()

    if args.blink:
        blink()
