#!/usr/bin/env python3
"""Git status or update for all of my git repos in a folder."""

from pathlib import Path
import argparse
import os
from subprocess import call
from git import Repo


MY_REPO_STARTSWITH = "git@neuromorphometrics.org"

C0 = "\x1b[0m"  # Reset
CR = "\x1b[38;5;9m"  # Red bold
CG = "\x1b[38;5;10m"  # Green
CY = "\x1b[38;5;11m"  # Yellow
CP = "\x1b[38;5;12m"  # purple
CB = "\x1b[38;5;27m"  # Blue
CM = "\x1b[38;5;13m"  # Magenta 13
CC = "\x1b[38;5;14m"  # Cyan
CO = "\x1b[38;5;202m"  # Orange
CWR = "\x1b[38;5;231m\x1b[48;5;196m"  # White on Red


def do_it(cmd, override_dry_run=False):
    if args.verbose >= 0:
        print(f"{CP}{cmd}{C0}")
    if args.dry_run and not override_dry_run:
        print(f"{CR}Not really running{C0} {cmd}")
    else:
        return(call(cmd, shell=True))


def add_and_commit(repo):
    if args.dry_run:
        print(f"{CY}Not adding/committing (dry run){C0}")
    else:
        print(f"{CC}git add .{C0}")
        msg = repo.git.add(".")
        print(msg)
        print(f"{CC}git commit -m update{C0}")
        msg = repo.git.commit("-m", "update")
        print(msg)
        print(f"{CC}git push{C0}")
        msg = repo.git.push()
        print(msg)


def main():

    exit_code = 0

    if args.verbose > 1:
        print(f"no telling what verbose is.  Okay it's {CG}{args.verbose}{C0}")
        print(f"args: {CY}{args}{C0}")

    cwd = Path().cwd()
    gits = Path(".").glob("*/.git")
    for git in gits:

        dir = git.parts[0]

        if args.verbose > 1:
            print(f"git = '{git}'")
            print(f"dir = '{dir}'")

        os.chdir(dir)

        repo = Repo(".")

        if args.verbose > 1:
            cmd = "ls -l"
            ret = do_it(cmd)
            print(f"ret = {CY}{ret}{C0}")

        if args.verbose >= 0:
            print(f"{CM}{dir}{C0}", end="")

        is_mine = False
        for remote in repo.remotes:
            for url in remote.urls:
                if url.startswith(MY_REPO_STARTSWITH):
                    is_mine = True
                    print(f" {CB}mine{C0}", end="")
                else:
                    if args.verbose > 0:
                        print(f" ? {url}", end="")

        if repo.is_dirty():

            print(f" {CO}dirty{C0}")

            if is_mine:

                cmd = "git status"
                ret = do_it(cmd, override_dry_run=True)

                if args.add: # don't ask, just add it
                    add_and_commit(repo)

                elif input("Add . and commit [y/N]? ") in ["y", "Y", "yes"]:
                    add_and_commit(repo)

                else:
                    print(f"not adding/committing {dir}")

        else:
            print(f" {CG}clean{C0}")

        os.chdir(cwd)

    return exit_code


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=__doc__)
    # parser.add_argument("src",help="(old) source test direcory name (no path)")
    # parser.add_argument("dst",help="(new) destination directory name")
    # parser.add_argument('--session-id', action='store', nargs='*', type=str,
    #                    help='filter input dataset by session id')
    # parser.add_argument('-c', '--compare', nargs=2, metavar=('new', 'old'),
    #                    help='Compares previous run results in oldfile with ' +\
    #                    'actual run results in newfile.')
    parser.add_argument("-a", "--add", action="store_true", help="Add everything and commit")
    parser.add_argument("-d", "--dry_run", action="store_true", help="Don't actually do anything, just print what would have been done")
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument("-q", "--quiet", action="store_true", help="Limit output")

    args = parser.parse_args()

    if args.quiet:
        args.verbose = -1

    exit_code = main()

    os.sys.exit(exit_code)
