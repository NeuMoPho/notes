#!/usr/bin/env python3
"""
SYNOPSIS

    Template.py [options]

DESCRIPTION

    Does such and such with so and so you know

EXAMPLES

    TODO

AUTHOR

    Andrew J. Worth 
    andy@neuromorphometrics.com
         
    Neuromorphometrics, Inc.
    22 Westminster Street
    Somerville, MA  02144-1630  USA
     
    http://Neuromorphometrics.com
    
LICENSE
    (c) Copyright 2019 Neuromorphometrics, Inc.   All rights reserved.

VERSION
"""

import sys
import os
import traceback
import optparse
import time
from datetime import timedelta, datetime
import xml.etree.ElementTree as ET
import glob

GVersion='$Id: Template.py,v 0.0 2013/04/11 04:19:15 andy Exp $'
GStartTime = time.time()
GVerboseLevel = 0

#-----------------------------------------------------------------------------
# See https://en.wikipedia.org/wiki/ANSI_escape_code
def PrintInColor(f,b,t,e):
  if f > -1:
    print('\x1b[38;5;'+str(f)+'m', end='')   # set Foreground
  if b > -1:
    print('\x1b[48;5;'+str(b)+'m', end='')   # set Background
  print(t+'\u001b[0m', end=e)                # Reset
  #print('\x1b[38;5;0m', end='')   # Black Foreground
  #print('\x1b[48;5;231m', end='') # White Background

  #PrintInColor(2,-1,'yes','\n') # green
  #PrintInColor(9,-1,'no','\n') # red
  #PrintInColor(129,-1,'purple info','\n') # purple
  #PrintInColor(12,-1,'blue info','\n') # blue
  #PrintInColor(202,-1,'warning','\n') # orange
  #PrintInColor(231,196,' error ','') ; print('') # white on red

#-----------------------------------------------------------------------------
def DoIt(cmd):
  if args.verbose >= 0:
    PrintInColor(129,-1,cmd,'\n') # purple
  if options.dryrun:
    print('NOT REALLY RUNNING '+cmd)
  else:
    os.system(cmd)

#-----------------------------------------------------------------------------
def GetIt(cmd):
  if args.verbose >= 0:
    PrintInColor(129,-1,cmd,'\n') # purple
  if options.dryrun:
    cmdOutput = 'NOT REALLY RUNNING '+cmd
  else:
    aFile = os.popen(cmd)
    cmdOutput = aFile.read()
    aFile.close()
  return cmdOutput 

#-----------------------------------------------------------------------------
def ComplainAndDie(msg):
    PrintInColor(9,-1,msg,'\n') # red
    print('Exiting Early.')
    sys.exit(0)

#===============================================================================
def main ():

  global options, args, GVersion, GStartTime, GVerboseLevel

  # I'm just wondering who I really am
  FullPathToMe = os.path.realpath(__file__)
  #print("FullPathToMe ", FullPathToMe)
  SplitFullPathToMe = os.path.split(FullPathToMe)
  PathToMe = SplitFullPathToMe[0]
  ME       = SplitFullPathToMe[1]
  sPathToMe = PathToMe.split('/')
  ResPath = '/'.join(sPathToMe[:-1])+'/../'
  CurWurkDur = os.getcwd() # Where I'm being run from

  if args.verbose >= 2:
    print('  I am "%s"' % ( ME ))
    print('  Some call me "%s"' % ( __name__ ))
    print('  I am at "%s"' % ( PathToMe ) )
    print('  I was run from "%s"' % ( CurWurkDur ))

  if args.verbose >= 2:
    print('Number of arguments:', len(sys.argv))
    print('Argument List:', str(sys.argv))

  # Get required command line arguments
  numRequiredArgs = 2
  if len(sys.argv) < numRequiredArgs+1:
    print(__doc__)
    msg='I am so sad.  I miss my required arguments.'
    PrintInColor(9,-1,msg,'\n') # red
    print('I quit.')
    sys.exit(0)
  else:
    arg1 = args[0]
    arg2 = args[1]

  # Echo arguments
  if args.verbose >= 2:
    print(f'arg1 : {arg1}')
    print(f'arg2 : {arg2}')

  #-------------------------------------------------------------------------
  if args.verbose >= 1:
    print(ME+' Doing something')

  cmd = 'echo Boyhowdy '
  DoIt(cmd)

  cmd = 'echo Boyhowdy '
  out = GetIt(cmd)
  print(out)


  #PrintInColor(231,196,' Exiting Early ','') ; print('') # white on red
  #sys.exit(0)

  if args.verbose >= 1:
    print(ME+' End of main()')

#---------------------------------------------------------------------------
# Call Main Program (above) with exceptions, etc.
#---------------------------------------------------------------------------
# Uncomment the following section if you want readline history support.
#import readline, atexit
#histfile = os.path.join(os.environ['HOME'], '.RunAll_history')
#try:
#    readline.read_history_file(histfile)
#except IOError:
#    pass
#atexit.register(readline.write_history_file, histfile)
if __name__ == '__main__':
  try:
    parser = optparse.OptionParser(
                formatter=optparse.TitledHelpFormatter(),
                usage=globals()['__doc__'],
                version=GVersion)
                #version='$Id: RunAll.py,v 1.4 2013/04/11 04:19:15 andy Exp $')
    parser.add_option ('-d', '--dryrun', action='store_true',
                default=False, help="don't execute, just show what doing")
    parser.add_option ('-q', '--quiet', action='store_true',
                default=False, help='suppress output')
    parser.add_option ('-v', '--verbose', action='store_true',
                default=False, help='verbose output')
    parser.add_option ('-V', '--Verbose', action='store_true',
                default=False, help='more verbose output')
    parser.add_option ('-l', '--verboseLevel', action='store', type="int",
                default=0, help='level of verbose output')
    (options, args) = parser.parse_args()
    #if len(args) < 1:
    #    parser.error ('missing argument')
    GVerboseLevel = options.verboseLevel
    if options.quiet:
      GVerboseLevel = -1
    if options.verbose:
      if GVerboseLevel < 1: GVerboseLevel = 1
    if options.Verbose:
      if GVerboseLevel < 2: GVerboseLevel = 2
    if GVerboseLevel > 0:
      print('Running: '+__file__)
      print(time.ctime(GStartTime))
      print("VerboseLevel is "+str(GVerboseLevel))
    exit_code = main() # DoItToIt!
    if exit_code is None:
      exit_code = 0
      tt = 'Total time: %s' % timedelta(seconds=(time.time() - GStartTime))
      if GVerboseLevel >= 1:
        print(time.asctime())
        print(tt)
    sys.exit(exit_code)
  except KeyboardInterrupt as e: # Ctrl-C
    raise e
  except SystemExit as e: # sys.exit()
    raise e
  except Exception as e:
    print('ERROR, UNEXPECTED EXCEPTION')
    print(str(e))
    traceback.print_exc()
    os._exit(1)

# vi:set autoindent ts=2 sw=2 expandtab : See Vim, :help 'modeline'
